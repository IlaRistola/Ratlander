**RATLANDER**
Mobile game (Unity)
The game view is 2D, but takes advantage of 3d with effects and some assets.

-- Downloadable android package demo added --

Alpha test version: https://youtu.be/XVKc0WXDcno

Developer: Ila Ristola

Graphic Designer: Iiro Ristola


---Ratlander Game Design Document---


Overview 
           1.1 History
           1.2 What is Ratlander?
           1.3 Goals

Gameplay
           2.1 Touch-inputs
           2.2 Flying mechanic
           2.3 Ratbooster
           2.4 Hitpoints/ Crash and collect
           2.5 Start menu
           2.6 Continue

Chapters

Environments
           4.1 Static
                 4.1.1 Chapter 1
                 4.1.2 Chapter 2
           4.2 Animated/interactive
                 4.2.1 Chapter 1
                 4.2.2 Chapter 2

Character

Assets
           6.2 Chapter 1
           6.2 Chapter 2

UI
           7.1 Touchable options
           7.2 Design
           7.3 Art

Lights

Audio
           8.1 Background music
           8.2 Effects

Game engine etc.

Assignments


Updates to GDD(date&note):



Overview

1.1.History

We start do Ratlander with javascript couple years ago. It was a little demo with some play mechanic and crashing option.
We decide move game to Unity and start from beginning on there.
First aspect for the game mechanic was hit/slide the mobile screen for keep airplane on the air. Then we made kind of gasoline mechanic to the game, so when gasoline is empty airplane got heavy mass and it’s probably fall down.
Later we decide make flying mechanic more easier, with touch-slide up = plane go up/touch-slide down = plane go down etc. We think it is more fun, if plane not fall down all the time. Of course still when gasoline(ratbooster) is empty, the airplane start falling more.
On first unity version we use “crash and collect”(if player hit some small assets) for gain more gasoline. Later we decide, that “crash and collect” is more like feature to collect points for perfect fly. So now we collect more gasoline with airballoon(?).


1.2 What is Ratlander

Ratlander is kind of flying-survival game, where player need to keep plane on air till to the goal(landing). Same time game try to excite player take some risk for the perfect fly.
Idea of name Ratlander kind of coming from movie series “Highlander”. Ratlander character coming also from scotland, same as Mcloud on Highlander.
Other aspect from the movie, is that Rat is also almost mortal. We fall thousand time and still survive.
“There can be only one - Highlander” -> There can be only one - RatLander”
Platform for the came is android tablet and cell phones. Ios later. There is also possibility make web browser versio, and use mouse or buttons for control.
Leveling in the game is linear. When player accomplish chapter next open.

Our new aspect for the game publishing is been more like made this commercial game for some company. Idea is the now polish demo with about couple levels and trying to see, if find any interested co-partners. 

1.3 Goals

As we say purpose of the game is the survive to the end of chapter. Ending must be that funny and interesting, so player want to see it, and know to wait it. There is cartoon comics elements on the end(no gameplay need at this point).
For open another chapter player must survive to end and collect at least one star, so player need to take some risk. Only collect RatBoost-palloons not enough.
Chapters should also be that interesting and diversity, that player one see another one. That’s why we are planning chapters like refuse dump and office etc. = we can use different crashable assets on different chapters, for bring to diversity.



Gameplay

Gameplay mode is manipulated 2D, there is some 3d elements like flying crashable parts and in point system.
Plaeyr and camera move when playing, environments and assets(before hits) keep positions. Player move up and down on screen, but camera keep same horizontal position all the time.
When player crash (not crashable objects) game change to continue screen what include some cartoon element.

2.1 Touch-input

We use mobile touch-input for menus and on gameplay.
For flying we make up/down controller front of the airplane. There is no need for forward/backward feature on this time.
Feeling of touch button must be smooth, clear and precise. So player got feeling that he control the plane and are able to take some risk for crashable items.

2.2 Flying mechanic

There is scripted movement speed, rotation and up/down force for the player.
When gasoline starting to empty plane coming more heavy and start to fall down. There is ‘change mass’ script for that.

2.3 Ratbooster

Ratbooster is gasoline for airplane. Rat can collect boost from airballoons. Airballoons can be casually found from play scene.
When player hit the balloons, gasoline meter jump full. Airballoons pop/fade away after hits.

2.4 Hitpoints/ Crash and collect

Every chapters include three kind crashable items for collect points. Items give different points, which depends on challenging and rareness. Player see defined collectable items before chapter. Every chapters has own items, which depend chapter theme. Items are better to be funny!
Items has own mass, rigidbody and collider, so they acting different after crash. For next chapter player have to collect at least some hitpoints(at least one star), so game want player take some risks.
Points-system: 

2.5 Start menu

There is map for chapters on the start menu. Locked chapters are grey, or with lock pic. Option menu not yet needed for menu. If player are already played chapter, stars are showed under chapter.
If player touch chapter icon, another chapter start. “Play again” are on like to have list.

2.6 Continue

Continue-screen show up when player crash to static/non-crashable elements. 
There is continue option for “continue” and “main menu”. Pressing continue start chapter from beginning.
Continue/crash screen shows cartoon style picture, where rat is crashed, but still alive.

2.7 Crashing

Crash-Bang picture show up if Rat crash to static enviroments, or certain non-crashable assets. After crash-bang asset(rat front-view) fly to the front of screen lens, which open continue screen. There can be couple sprite in rat asset(like to have).


Chapters

(More chapters coming later….)
Last chapter of the game should be on Scotland, like a way back home. 

3.1 Dump
-Iiro writes something about visual look and feeling etc. *voit kirjottaa myös suomeksi. Kääntelee sitten englanniksi,kun ehtii.*

3.2 ???????


Environments


4.1 Static
 
4.1.1 Chapter 1

4.1.2 Chapter 2

4.2 Animated/interactive

4.2.1 Chapter 1

4.2.2 Chapter 2


Character

Main character is Rat, who flying airplane. Airplane looks like homemade, with recycling bottle -body and other stuff.
On now Rat is same sprite all the time, but there is some ideas to bring more sprites for the Rat. Atleast Rat got scarf what move when player fly.
Rat looks alike cartoon character.
If there is some kind role model to Rat, it must be John Belush’s crazy pilot, in movie “Good morning Vietnam”.
As i talks before, Rat is almost mortal character, who crash many times and survive always -> ‘continue’.
Rat homeland should be some where in Scotland, just like ‘Highlander’ movie.


Assets

Chapter 1

Chapter 2


UI

