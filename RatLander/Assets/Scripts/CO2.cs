﻿using UnityEngine;
using System.Collections;

public class CO2 : MonoBehaviour
{

    public static int scoreValue;
    public PointSystem pointSystem;
    public AudioSource CO2Audio;
    public AudioClip[] CO2Clips;
    public GameObject player;

    void Start()
    {
        CO2Audio = gameObject.GetComponent<AudioSource>();
    }

    void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Rat")
        {
            //GetComponent<PolygonCollider2D>().enabled = false;
            Destroy(this.gameObject, 7);
            GetComponent<AudioSource>().Play();
            Physics2D.IgnoreCollision(player.GetComponent<Collider2D>(), GetComponent<Collider2D>());
        }
        if (collision.gameObject.tag == "CO")
        {
            GetComponent<PolygonCollider2D>().enabled = false;
            //scoreValue += 10;
            CO2Audio.clip = CO2Clips[Random.Range(0, CO2Clips.Length)];
            CO2Audio.Play();
        }
        if (collision.gameObject.tag == "CO2")
        {
            GetComponent<PolygonCollider2D>().enabled = false;
            scoreValue += 20;


        }
        if (collision.gameObject.tag == "CO3")
        {
            GetComponent<PolygonCollider2D>().enabled = false;
            scoreValue += 80;
            CO2Audio.clip = CO2Clips[Random.Range(0, CO2Clips.Length)];
            CO2Audio.Play();

        }
       
    }

}