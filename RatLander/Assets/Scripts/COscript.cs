﻿using UnityEngine;
using System.Collections;

public class COscript : MonoBehaviour
{
    public static int scoreValue;
    public PointSystem pointSystem;

    Vector3 lastPos;

    float threshold = 0.3f;


    void Start()
    {
        lastPos = gameObject.transform.position; 
    }

    void FixedUpdate()
    {
        Vector3 offset = gameObject.transform.position - lastPos;

        if (offset.x > threshold)
        {
            GetComponent<BoxCollider2D>().enabled = false;
            Destroy(this.gameObject, 5);
            scoreValue += 10;
        }
        
         else if (offset.x < -threshold)
        {
            GetComponent<BoxCollider2D>().enabled = false;
            Destroy(this.gameObject, 5);
            scoreValue += 10;
        }
    }

    void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Rat" || collision.gameObject.tag == "CO2")
        {
            GetComponent<BoxCollider2D>().enabled = false;
            Destroy(this.gameObject, 5);
            GetComponent<AudioSource>().Play();
            scoreValue += 20;

        }

    }
    
    
}
