﻿using UnityEngine;
using System.Collections;

public class Crash : MonoBehaviour {

	public SpriteRenderer sprender;

	void Start()
	{
		sprender.enabled = true;
	}


	void turnOff() {
		if(!gameObject.CompareTag("Element"))
		{
			sprender = gameObject.GetComponent<SpriteRenderer>();
			sprender.enabled= false;
		}
	}
}