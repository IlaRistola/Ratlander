﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class RatCrash : MonoBehaviour {

    public GameObject rat;
    public GameObject flyingrat;
    //public AudioSource ratCra;
    //public AudioClip ratClip;

    void Start()
    {
        flyingrat.gameObject.SetActive(false);
        //ratCra = GetComponent<AudioSource>();
    }

    void Update()
    {
        if (rat == null)
        {
            flyingrat.gameObject.SetActive(true);
            //ratCra.PlayOneShot(ratClip, 1);
        }
    } 
}
