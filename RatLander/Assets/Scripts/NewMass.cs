﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class NewMass : MonoBehaviour {
	public float mass;
    public Rigidbody2D rb;
	public Slider healthBarSlider;
    /*
    public AudioSource gaugeSounds;
    public AudioClip[] EmptySpeaks;
    public AudioClip GasSpeaks;
    */
	
	void Start() {
        rb = GetComponent<Rigidbody2D>();
		rb.mass = mass;
        rb.mass = 1.6f;
        //gaugeSounds = gameObject.GetComponent<AudioSource>();

	}
	
	void Update() {
		/* 
        if (healthBarSlider.value < 0.50) {
			//weight.SetActive (true);
			//newmass.enabled = true;
			rb.mass = 2.5f;
		} /*
		if (healthBarSlider.value > 0.50) {
			//newmass.enabled = false;
			rb.mass = 1.6f;
		} *//*
       if (healthBarSlider.value == 25)
        {
            gaugeSounds.PlayOneShot(GasSpeaks, 1);
        } */
		if (healthBarSlider.value == 0) {
			rb.mass = 6;
		}
        else
        {
            rb.mass = 1.6f;
        }
		//if (healthBarSlider.value > 0.05) {
		//	rb.mass = 2.5f;
		//}
		
	}

    /*
    void SoundEffects()
    {
        if (healthBarSlider.value < 0.25) {
            gaugeSounds.clip = gaugeSpeaks[Random.Range(0, gaugeSpeaks.Length)];
            gaugeSounds.Play();
        }
    } */
}