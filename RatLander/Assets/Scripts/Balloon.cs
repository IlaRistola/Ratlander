﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class Balloon : MonoBehaviour {

    public Slider healthBarSlider;

    Rigidbody2D balloon;

    
    void Start()
    {
        balloon = GetComponent<Rigidbody2D>();
        balloon.Sleep();
       
       

    }

 

    void OnTriggerEnter2D(Collider2D colla)
    {
        if (colla.gameObject.tag == "Balloontrig")
        {
            balloon.WakeUp();
            balloon.velocity = Vector3.right;
        }
        if (colla.gameObject.tag == "Rat")
        {
            healthBarSlider.value = 100;
            Destroy(gameObject);
        }
        
       
    }
    
   
}
