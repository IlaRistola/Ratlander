﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PointSystem : MonoBehaviour {

    public GameObject star;
    public GameObject starTwo;
    public GameObject starThree;
    public int score; //can change private later?

    

	void Start () {

        score = 0;
        star.SetActive(false);
        starTwo.SetActive(false);
        starThree.SetActive(false);
        
	}

    void Update()
    {
        score = DestroyByContact.scoreValue;
        if (score > 134) //134
        {
            star.SetActive(true);
            //starSounds.clip = starClips[Random.Range(1, starClips.Length)];
            //starSounds.Play(); We need to add some sound effect here too
        } 
        
        if (score > 267)
        {
            starTwo.SetActive(true);
        }
        if (score > 400)
        {
            starThree.SetActive(true);
        }
            
    } 
}
