﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class TunnelWay : MonoBehaviour {

    public AudioSource startSounds;
    public AudioClip[] startSpeaks;

    void Start()
    {
        startSounds = gameObject.GetComponent<AudioSource>();
    }



    void OnTriggerExit2D(Collider2D starter)
    {
        if (starter.gameObject.tag == "Startpipe")
        {
            GameObject.FindGameObjectWithTag("Rat").GetComponent<Rat>().enabled = true;
            startSounds.clip = startSpeaks[Random.Range(0, startSpeaks.Length)];
            startSounds.Play();
            
        }
    }
    /*
    void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Tunnel")
        {
            //Debug.Log("are you ready");
            GameObject.FindGameObjectWithTag("Rat").GetComponent<Rat>().enabled = false;
        }
        
        if (collision.gameObject.tag == "Endpipe")
        {
            GameObject.FindGameObjectWithTag("Rat").SetActive(false);
            //WaitForSeconds (7);
            //Application.LoadLevel("Ratlander1");
            StartCoroutine(WaitForIt(5.0F));
        }
    }

    IEnumerator WaitForIt(float waitTime)
    {
        yield return new WaitForSeconds(waitTime);
        SceneManager.LoadScene("Ratlander1");
    } */
}
