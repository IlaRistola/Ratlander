﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class MainMenu : MonoBehaviour {

    public Button toJunkyard;
    public Button toJunkytwo;
    public GameObject note;

	// Use this for initialization
	void Start () {
        toJunkyard.onClick.AddListener(TaskOnClick);
        toJunkytwo.onClick.AddListener(TaskOnClickT);
        note.gameObject.SetActive(false);
        GetComponent<AudioSource>().Play();
	}
	
	void TaskOnClick()
    {
        note.gameObject.SetActive(true);
    }
    
    void TaskOnClickT()
    {
        SceneManager.LoadScene("Ratlander1");
    } 
}
