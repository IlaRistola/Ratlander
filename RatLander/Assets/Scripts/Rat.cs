﻿using UnityEngine;
using System.Collections.Generic;

public class Rat : MonoBehaviour
{


    public float speed;

    public float force;

    public float tilt;

    public AudioSource ratAudio;
    public AudioClip[] ratVCO2;
    public AudioClip ratUp;
    public AudioClip[] ratHitBall;


    Rigidbody2D body;
    Transform trans;
    public float rotatePowerUp, rotatePowerDown;
    private float rotatePowerUp_P, rotatePowerDown_P;
    public float rotateUpLimit, rotateDownLimit;
    Vector3 euler;
    public float angular;
    bool pressed;

    void Start()
    {

        GetComponent<Rigidbody2D>().velocity = Vector3.right * speed;
        body = GetComponent<Rigidbody2D>();
        trans = transform;
        euler = trans.eulerAngles;
        //
        resetVariables();
        ratAudio = gameObject.GetComponent<AudioSource>();

    }

    void resetVariables()
    {
        rotatePowerUp_P = rotatePowerUp;
        rotatePowerDown_P = rotatePowerDown;
    }

    void rotateDown()
    {

        euler = trans.eulerAngles;
        if (euler.z > 280 && angular > 20)
        {
            angular = 10;
        }
        else if (euler.z < 280)
        {
            if (angular > -rotateDownLimit)
            {

                rotatePowerDown_P += 200 * Time.deltaTime;
                // the parenthesis below should return either -1 or 1.
                angular += (euler.z < 120 ? -1 : 1) * rotatePowerDown_P * Time.deltaTime;
            }
            body.angularVelocity = angular;
        }
        resetVariables();
    }

    void rotateUp()
    {
        euler = trans.eulerAngles;

        if (angular < rotateUpLimit)
        {
            if (angular < 0)
            {
                rotatePowerUp_P += 300 * Time.deltaTime;
            }
            else
            {
                rotatePowerUp_P += 100 * Time.deltaTime;
            }
            angular += rotatePowerUp_P * Time.deltaTime;
        }
        body.angularVelocity = angular;
    }


    void Update()
    {
        if (SwipeManager.IsSwipingUp())
        {
            GetComponent<Rigidbody2D>().AddForce(Vector3.up * force);
            GetComponent<Rigidbody2D>().rotation = GetComponent<Rigidbody2D>().velocity.x * -tilt;
            ratAudio.PlayOneShot(ratUp, 0.5f);
            {
                rotateUp();
                
            }
        }
        if (SwipeManager.IsSwipingDown())
        {
            GetComponent<Rigidbody2D>().AddForce(Vector3.down * force);
            GetComponent<Rigidbody2D>().rotation = GetComponent<Rigidbody2D>().velocity.x * +tilt;
            {
                rotateUp();

            }
        }

    

        if (SwipeManager.swipeEnded) {
            
            rotateDown ();
            resetVariables();
                }
	}

    void OnCollisionEnter2D(Collision2D player)
	{
		if (player.gameObject.tag == "CO" || player.gameObject.tag == "CO2") {
            GetComponent<Rigidbody2D>().velocity = Vector3.right * speed;
		}
        if (player.gameObject.tag == "CO3")
        {
            this.gameObject.GetComponent<Rigidbody2D>().velocity = Vector3.right * speed;
            ratAudio.clip = ratVCO2[Random.Range(0, ratVCO2.Length)];
            ratAudio.Play();
        }
        if (player.gameObject.tag == "EndBorder")
        {
            this.gameObject.GetComponent<Rigidbody2D>().velocity = Vector3.right * speed;
        }
        
	}

    void OnTriggerEnter2D(Collider2D play)
    {
        if (play.gameObject.tag == "Bubble")
        {
            ratAudio.clip = ratHitBall[Random.Range(0, ratHitBall.Length)];
            ratAudio.Play();
        }
    }
    
	
    
}

     
	