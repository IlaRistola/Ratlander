﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GaugeScript : MonoBehaviour {
	
	public Slider healthBarSlider;
	public Text lowText;
	public Text emptyText;
    public Button restart;
    public Button exit;
    public GameObject rat;
   
	

	void Start(){
		lowText.enabled = false;
		emptyText.enabled = false;
        restart.gameObject.SetActive(false);
        restart.onClick.AddListener(TaskOnClick);
        exit.gameObject.SetActive(false);
        exit.onClick.AddListener(TaskOnChic);
       
	}
    
    void Update()
    {
        if (rat == null)
        {
            StartCoroutine(WaitForIt(4.0F));
            
        } 
       
    }
    

    void FixedUpdate()
    {
        healthBarSlider.value -= 2.5f * Time.deltaTime;
        if (healthBarSlider.value < 25 && healthBarSlider.value > 1)
        {
            lowText.enabled = true;
        }
        else
        {
            lowText.enabled = false;
        }
        if (healthBarSlider.value == 0)
        {
            emptyText.enabled = true;
            GetComponent<AudioSource>().enabled = true;
        }
        else
        {
            emptyText.enabled = false;
            GetComponent<AudioSource>().enabled = false;
        }
     
        
    }
 

    IEnumerator WaitForIt(float waitTime)
    {
        yield return new WaitForSeconds(waitTime);
            restart.gameObject.SetActive(true);
            exit.gameObject.SetActive(true);
    }

    void TaskOnClick()
    {
        SceneManager.LoadScene("Ratlander1");
    }

    void TaskOnChic()
    {
        SceneManager.LoadScene("StartScreen");
    }


}
