﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EndingTunnel : MonoBehaviour {

    public AudioSource endAudio;
    public AudioClip[] endSpeak;

	void Start () {
        endAudio = gameObject.GetComponent<AudioSource>();
	}

    void OnTriggerEnter2D(Collider2D collic)
    {
        if (collic.gameObject.tag == "Rat")
        {
            endAudio.clip = endSpeak[Random.Range(0, endSpeak.Length)];
            endAudio.Play();
        }

    }
}
