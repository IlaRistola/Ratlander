﻿using UnityEngine;
using System.Collections;

public class CameraFollow : MonoBehaviour
{
    // The Target
    public Transform target;

	public bool bounds;

	public Vector3 minCameraPos;
	public Vector3 maxCameraPos;


    // Update is called once per frame
    void LateUpdate()
    {

        if (target != null)
        {
            transform.position = new Vector3(target.position.x +7,
                                             target.position.y -2,
                                             transform.position.z
                                             );
            if (bounds)
            {
                transform.position = new Vector3(Mathf.Clamp(transform.position.x, minCameraPos.x, maxCameraPos.x),
                    Mathf.Clamp(transform.position.y, minCameraPos.y, maxCameraPos.y),
                    Mathf.Clamp(transform.position.z, minCameraPos.z, maxCameraPos.z));
            }
        }
		/*if (bounds) {
			transform.position = new Vector3 (Mathf.Clamp (transform.position.x, minCameraPos.x, maxCameraPos.x),
				Mathf.Clamp (transform.position.y, minCameraPos.y, maxCameraPos.y));
		}*/


        
       
    }

}

