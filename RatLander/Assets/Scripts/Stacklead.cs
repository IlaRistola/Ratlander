﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Stacklead : MonoBehaviour {

    

    void OnTriggerEnter2D(Collider2D slead)
    {
        if (slead.gameObject.tag == "Balloontrig")
        {
            transform.GetChild(0).gameObject.SetActive(true);
        }
    }

    void OnTriggerExit2D(Collider2D slead)
    {
        if (slead.gameObject.tag == "Balloontrig")
        {
            transform.GetChild(0).gameObject.SetActive(false);
            gameObject.SetActive(false);
        }
    }
}
