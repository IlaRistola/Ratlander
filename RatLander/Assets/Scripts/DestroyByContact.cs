﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class DestroyByContact : MonoBehaviour
{
	public GameObject explosion;
    public static int scoreValue; //static is maybe little dull, but let see..
    public PointSystem pointSystem;
	private bool backtoScreen;

    

    
    void Start()
    {
		scoreValue = 0;
    }


    void OnCollisionEnter2D(Collision2D collision) 
	{
		if (collision.gameObject.tag == "CO")
		{
            scoreValue += 10;
			return;
		}
        if (collision.gameObject.tag == "CO2")
        {
            scoreValue += 20;
            return;
        }
        if (collision.gameObject.tag == "CO3")
        {
            scoreValue += 80;
            return;
        }
        if (collision.gameObject.tag == "GR" || collision.gameObject.tag == "Bubble" || collision.gameObject.tag == "EndBorder")
		{
			return;
		}

        else
        {
            scoreValue = 0;
        }
		Instantiate(explosion, transform.position, transform.rotation);
		Destroy(gameObject);
        
	}
    
}