﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundController : MonoBehaviour {

    public GameObject rat;
    public AudioSource bgAudio;
    public AudioClip[] bgMusic;

	// Use this for initialization
	void Start () {
        bgAudio = gameObject.GetComponent<AudioSource>();
        bgAudio.clip = bgMusic[Random.Range(0, bgMusic.Length)];
        bgAudio.Play();
	}

    void Update()
    {
        if (rat == null)
        {
            bgAudio.Stop();
        }
    }
	
	
}
