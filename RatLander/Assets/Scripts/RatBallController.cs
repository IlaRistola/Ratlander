﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class RatBallController : MonoBehaviour {

    public Slider healthBarSlider;
    public float Bscale = 2.4f;

 


    void Update()
    {   
        transform.localScale = new Vector3(Bscale, Bscale, 1);
    }
    
    public void AdjustScale(float newScale)
    {
        newScale = 1 +(newScale * 1.4f / 100);
        Bscale = newScale;
    }
 
} 
