﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class EndingPipe : MonoBehaviour {
	


    void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Rat")
        {
            //Debug.Log("are you ready");
            GameObject.FindGameObjectWithTag("Rat").GetComponent<Rat>().enabled = false;
            GetComponent<AudioSource>().Play();
        }
        if (collision.gameObject.tag == "Rat")
        {
            GameObject.FindGameObjectWithTag("Rat").SetActive(false);
            StartCoroutine(WaitForIt(5.0F));
        }
    }

    IEnumerator WaitForIt(float waitTime)
    {
        yield return new WaitForSeconds(waitTime);
        SceneManager.LoadScene("StartScreen");
    }
}
